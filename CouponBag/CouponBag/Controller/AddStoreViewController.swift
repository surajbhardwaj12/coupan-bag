//
//  AddStoreViewController.swift
//  Coupon Bag
//
//  Created by MAC OS 17 on 15/02/22.
//

import UIKit

class AddStoreViewController: UIViewController {

  
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        title = "Add Store Details"
        setMyNavigation(titleColor: .white, color: UIColor(named: "BtnColor")!, titleSize: 25)
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
