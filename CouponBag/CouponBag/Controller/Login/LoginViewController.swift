//
//  LoginViewController.swift
//  Coupon Bag
//
//  Created by MAC OS 17 on 20/01/22.
//

import UIKit
import TextFieldEffects

class LoginViewController: UIViewController {
    
    //MARK: - Variables
    var userid  = ""
    var fullName = ""
    var email = ""
    var uimage = ""
    
    //MARK: - Outlet
    @IBOutlet weak var txtEmail: HoshiTextField!
    @IBOutlet weak var btnSignup: UIButton!
    @IBOutlet weak var btnForgetPassword: UIButton!
    @IBOutlet weak var btnSignin: UIButton!
    @IBOutlet weak var txtPassword: HoshiTextField!
    @IBOutlet weak var viewEmail: UIView!
    @IBOutlet weak var viewPassword: UIView!
    // MARK: - Methods
    private func validateData() -> Bool {
        
        guard let email = self.txtEmail.text?.trimmingCharacters(in: .whitespacesAndNewlines),email.count > 0 else{
//                     SAAlertBar.show(.error, message:"Please enter your email to login".localizedLowercase)
                      return false
             }
       guard let password = self.txtPassword.text?.trimmingCharacters(in: .whitespacesAndNewlines),password.count > 0 else{
//                     SAAlertBar.show(.error, message:"Please enter your password to login")
                     return false
                }
        if !self.isValidEmail(testStr: txtEmail.text!){
//                   SAAlertBar.show(.error, message:"Please enter valid email".localizedLowercase)
                   return false
               }
        
//        if !self.isValidPassword(testStr: password){
//                   SAAlertBar.show(.error, message:"for password minimum 8 characters & must contain one number and one special character".localizedLowercase)
//                   return false
//        }
        
        return true
    }
    
    func isValidEmail(testStr:String) -> Bool {
           let emailRegEx = "[A-Z0-9a-z]+([._%+-]{1}[A-Z0-9a-z]+)*@[A-Za-z0-9]+\\.([A-Za-z])*([A-Za-z0-9]+\\.[A-Za-z]{2,4})*"
           let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
           return emailTest.evaluate(with: testStr)
       }
    func isValidPassword(testStr:String)->Bool{
        let passwordRegEx = "^(?=.*[A-Za-z])(?=.*\\d)(?=.*[$@$!%*#?&])[A-Za-z\\d$@$!%*#?&]{8,}$"
        let passwordTest = NSPredicate(format:"SELF MATCHES %@", passwordRegEx)
        return passwordTest.evaluate(with: testStr)
    }
    func isKeyPresentInUserDefaults(key: String) -> Bool {
        return UserDefaults.standard.object(forKey: key) != nil
    }
    func pushToRootHomeViewController(){
        
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let VC  = storyboard.instantiateViewController(withIdentifier: "ContainerControllerVC") as! ContainerControllerVC
            let navigationController = UINavigationController(rootViewController:VC)
            let sceneDelegate = UIApplication.shared.connectedScenes
               .first!.delegate as! SceneDelegate
            sceneDelegate.window!.rootViewController = navigationController
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
//        viewEmail.makeround()
//        viewPassword.makeround()
//        btnSignin.btnRound()
        txtEmail.text = "maulikkumbhani23@gmail.com"
        txtPassword.text = "123456789"
    }
    // MARK: - Action Method
    @IBAction func btnSignUp(_ sender: Any) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "SignUpViewController") as! SignUpViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnLogin(_ sender: Any) {
        guard self.validateData() else {
            return
        }
      
        var deviceToken = String()
        
        if isKeyPresentInUserDefaults(key: "fcmToken") {
            deviceToken = UserDefaults.standard.object(forKey: "fcmToken") as! String
        }else{
            deviceToken = "1234"
        }
        print(deviceToken)
        
        let dict = [
            APIManager.ParameterKey.EMAIL : txtEmail.text!.lowercased(),
            APIManager.ParameterKey.PASSWORD : txtPassword.text!,
            APIManager.ParameterKey.DEVICE_ID : deviceToken
        ]
        APIRequestClient.shared.sendAPIRequest(requestType: .POST, queryString: URL_Login, parameter: dict as [String:AnyObject], isHudeShow: true, success: { (responseSuccess) in
           
            if let success = responseSuccess as? [String:Any] {
                if let userinfo = success["data"] as? [String:Any]{

                    DispatchQueue.main.async {
                        let userObject :UserDetail = UserDetail.init(userDetail: userinfo)
                        userObject.setuserDetailToUserDefault()
                        self.pushToRootHomeViewController()
//                        if self.parent == nil {
//                            self.dismiss(animated: true, completion:nil)
//                        }else{
//                           self.pushToRootHomeViewController()
//                        }
                    }
                }
            }else{
                DispatchQueue.main.async {
//                    SAAlertBar.show(.error, message:"\(kCommonError)".localizedLowercase)
                }
            }
        }) { (responseFail) in
            print(responseFail)
            if let failResponse = responseFail  as? [String:Any],let errorMessage = failResponse["error_data"] as? [String]{

                DispatchQueue.main.async {
                    if errorMessage.count > 0{
//                        SAAlertBar.show(.error, message:"\(errorMessage.first!)".localizedLowercase)
                    }
                }
            }else{
//                DispatchQueue.main.async {
////                    SAAlertBar.show(.error, message:"\(kCommonError)".localizedLowercase)
//                }
            }
        }
        print(dict)
            
        
    }
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */


