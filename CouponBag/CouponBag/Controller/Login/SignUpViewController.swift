//
//  SignUpViewController.swift
//  Coupon Bag
//
//  Created by MAC OS 17 on 02/02/22.
//

import UIKit
import TextFieldEffects
import CropViewController
import YPImagePicker



class SignUpViewController: UIViewController {
    
    
    // MARK: - Outlet
    @IBOutlet weak var txtName: HoshiTextField!
    @IBOutlet weak var txtEmail: HoshiTextField!
    @IBOutlet weak var txtMobileNumber: HoshiTextField!
    @IBOutlet weak var txtConfirmPassword: HoshiTextField!
    @IBOutlet weak var btnProfileImage: UIButton!
    @IBOutlet weak var txtPassword: HoshiTextField!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var txtPin: HoshiTextField!
    
    // MARK: - Variable
    var imagePickerController = UIImagePickerController()
    var imageForCrop : UIImage?
    var ProfileImageData:Data?
   
    
    // MARK: - Action Method
    @IBAction func btnSelectProfile(_ sender: Any) {
        presentCameraAndPhotosSelector()
    }
    
    @IBAction func btnSignUp(_ sender: Any) {
        guard self.Validate() else {
            return
        }
        var deviceToken : String?
        if isKeyPresentInUserDefaults(key: "fcmToken") {
            deviceToken = UserDefaults.standard.object(forKey: "fcmToken") as? String
        }else{
            deviceToken = "1234"
        }
        print(deviceToken)
        
        let dict = [
            APIManager.ParameterKey.NAME : txtName.text!,
            APIManager.ParameterKey.EMAIL : txtEmail.text!,
            APIManager.ParameterKey.CONTACT : txtMobileNumber.text!,
            APIManager.ParameterKey.PASSWORD : txtPassword.text!,
            APIManager.ParameterKey.PINCODE : txtPin.text!,
            APIManager.ParameterKey.DEVICE_ID : deviceToken
          
        ] as [String : Any]
        
        APIRequestClient.shared.sendAPIRequestImage(requestType: .POST, queryString: URL_Register, parameter: dict as [String:AnyObject] , imageData: ProfileImageData, isFileUpload: false, fileName: "test",filekey:APIManager.ParameterKey.IMAGE, isHudeShow: true) { (responseSuccess) in
            print(responseSuccess)
            
                if let userinfo = responseSuccess as? [String:Any]{
                    DispatchQueue.main.async {
                        let userObject :UserDetail = UserDetail.init(userDetail: userinfo)
                        userObject.setuserDetailToUserDefault()
                        self.pushToRootHomeViewController()
//                        if self.parent == nil {
//                            self.dismiss(animated: true, completion:nil)
//                        }else{
//                           self.pushToRootHomeViewController()
//                        }
                    }
                }
        } fail: { (failResponse) in
            print(failResponse)
        }
    }
    
    // MARK: - Custom Method
    
    func isKeyPresentInUserDefaults(key: String) -> Bool {
        return UserDefaults.standard.object(forKey: key) != nil
    }
    
    func presentCameraAndPhotosSelector(){
        //PresentMedia Selector
        let actionSheetController = UIAlertController.init(title: "", message: "Profile", preferredStyle: .actionSheet)
        let cancelSelector = UIAlertAction.init(title: "Cancel", style: .cancel, handler:nil)
        cancelSelector.setValue(UIColor(named:"38B5A3"), forKey: "titleTextColor")
        
        actionSheetController.addAction(cancelSelector)
        let photosSelector = UIAlertAction.init(title: "Photos", style: .default) { (_) in
            DispatchQueue.main.async {
                self.imagePickerController = UIImagePickerController()
                self.imagePickerController.sourceType = .savedPhotosAlbum
                self.imagePickerController.delegate = self
                self.imagePickerController.allowsEditing = false
                //self.imagePickerController.mediaTypes = [kUNT as String]
                self.view.endEditing(true)
                self.presentImagePickerController()
            }
        }
        photosSelector.setValue(UIColor(named:"38B5A3"), forKey: "titleTextColor")
        actionSheetController.addAction(photosSelector)
        
        let cameraSelector = UIAlertAction.init(title: "Camera", style: .default) { (_) in
            if CommonClass.isSimulator{
                DispatchQueue.main.async {
                    let noCamera = UIAlertController.init(title:"Cameranotsupported", message: "", preferredStyle: .alert)
                    noCamera.addAction(UIAlertAction.init(title:"ok", style: .cancel, handler: nil))
                    self.present(noCamera, animated: true, completion: nil)
                }
            }else{
                DispatchQueue.main.async {
                    self.imagePickerController = UIImagePickerController()
                    self.imagePickerController.delegate = self
                    self.imagePickerController.allowsEditing = false
                    self.imagePickerController.sourceType = .camera
                    //self.imagePickerController.mediaTypes = [kUTTypeImage as String]
                    self.presentImagePickerController()
                }
            }
        }
        cameraSelector.setValue(UIColor(named:"38B5A3"), forKey: "titleTextColor")
        actionSheetController.addAction(cameraSelector)
        self.view.endEditing(true)
        self.present(actionSheetController, animated: true, completion: nil)
    }
    func presentImagePickerController(){
           self.view.endEditing(true)
           self.imagePickerController.modalPresentationStyle = .fullScreen
           self.present(self.imagePickerController, animated: true, completion: nil)
          
       }
    override func viewDidLoad() {
        super.viewDidLoad()
//        btnProfileImage.setRoundButton()
          imgProfile.setRoundImage()
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */


    func Validate() -> Bool {
        
        guard let _ = ProfileImageData else {
//            SAAlertBar.show(.error, message:"Please select a profile picture".localizedLowercase)
            return false
        }
        
        guard let name = txtName.text?.trimmingCharacters(in: .whitespacesAndNewlines),name.count > 0 else {
//            SAAlertBar.show(.error, message:"Please enter username".localizedLowercase)
            return false
        }
        
        guard let mailAddress = txtEmail.text?.trimmingCharacters(in: .whitespacesAndNewlines),mailAddress.count > 0  else {
//            SAAlertBar.show(.error, message:"Please enter your mail-address".localizedLowercase)
            return false
        }
        
        guard let contact = txtMobileNumber.text?.trimmingCharacters(in: .whitespacesAndNewlines),contact.count > 0  else {
//            SAAlertBar.show(.error, message:"Please enter your contact number ".localizedLowercase)
            return false
        }
        guard let securePin = txtPin.text?.trimmingCharacters(in: .whitespacesAndNewlines),securePin.count > 0  else {
//            SAAlertBar.show(.error, message:"Please enter secure pin for store".localizedLowercase)
            return false
        }
        guard let password = txtPassword.text?.trimmingCharacters(in: .whitespacesAndNewlines),password.count > 0  else {
//            SAAlertBar.show(.error, message:"Please enter your password".localizedLowercase)
            return false
        }
        
        guard let confirmPassword = txtConfirmPassword.text?.trimmingCharacters(in: .whitespacesAndNewlines),confirmPassword.count > 0  else {
//            SAAlertBar.show(.error, message:"Please re-enter your password".localizedLowercase)
            return false
        }
       
        guard securePin.count == 4  else {
//            SAAlertBar.show(.error, message:"Secure Pin must have 4 number".localizedLowercase)
            return false
        }
        guard password.count > 8 && password.count < 18  else {
//            SAAlertBar.show(.error, message:"Password must have minimum 8 and maximum 18 character ".localizedLowercase)
            return false
        }
        
        guard confirmPassword.count > 8 && confirmPassword.count < 18  else {
//            SAAlertBar.show(.error, message:"Conform password must have minimum 8 and maximum 18 character ".localizedLowercase)
            return false
        }
        
        
        
        if (txtEmail.text?.isEmpty) == false {
                  if !self.isValidEmail(testStr: txtEmail.text!){
//                      SAAlertBar.show(.error, message:"Please enter valid mail".localizedLowercase)
                      return false
                  }
        }
        
        if (txtMobileNumber.text?.isEmpty) == false {
                  if !self.isValidContact(testStr: txtMobileNumber.text!){
//                      SAAlertBar.show(.error, message:"Please enter valid phone".localizedLowercase)
                      return false
                  }
        }
        
        if password != confirmPassword {
//            SAAlertBar.show(.error, message:"Both Password are not matching".localizedLowercase)
            return false
        }
        
        return true
        
    }
    func resize(_ image: UIImage) -> Data{
        var actualHeight = Float(image.size.height)
        var actualWidth = Float(image.size.width)
        let maxHeight: Float = 900
        let maxWidth: Float = 900
        var imgRatio: Float = actualWidth / actualHeight
        let maxRatio: Float = maxWidth / maxHeight
        let compressionQuality: Float = 0.5
        //50 percent compression
        if actualHeight > maxHeight || actualWidth > maxWidth {
            if imgRatio < maxRatio {
                //adjust width according to maxHeight
                imgRatio = maxHeight / actualHeight
                actualWidth = imgRatio * actualWidth
                actualHeight = maxHeight
            }
            else if imgRatio > maxRatio {
                //adjust height according to maxWidth
                imgRatio = maxWidth / actualWidth
                actualHeight = imgRatio * actualHeight
                actualWidth = maxWidth
            }
            else {
                actualHeight = maxHeight
                actualWidth = maxWidth
            }
        }
        let rect = CGRect(x: 0.0, y: 0.0, width: CGFloat(actualWidth), height: CGFloat(actualHeight))
        UIGraphicsBeginImageContext(rect.size)
        image.draw(in: rect)
        let img = UIGraphicsGetImageFromCurrentImageContext()
        let imageData = img!.jpegData(compressionQuality: 0.3)
        UIGraphicsEndImageContext()
        return imageData!//UIImage(data: imageData!) ?? UIImage()
    }
    
    
    func isValidEmail(testStr:String) -> Bool {
           let emailRegEx = "[A-Z0-9a-z]+([._%+-]{1}[A-Z0-9a-z]+)*@[A-Za-z0-9]+\\.([A-Za-z])*([A-Za-z0-9]+\\.[A-Za-z]{2,4})*"
           let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
           return emailTest.evaluate(with: testStr)
       }
    
    func isValidContact (testStr:String) -> Bool {
        let phoneNumberRegex = "^[0-9]\\d{9}$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", phoneNumberRegex)
        let isValidPhone = phoneTest.evaluate(with: testStr)
        return isValidPhone
    }
}

extension SignUpViewController : UIImagePickerControllerDelegate,UINavigationControllerDelegate,CropViewControllerDelegate {
    
    func cropViewController(_ cropViewController: CropViewController, didCropToImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
        self.dismiss(animated: true, completion: nil)
        let resizedImage = self.resize(image)
        self.imgProfile.image =  UIImage.init(data: resizedImage)
        ProfileImageData = resizedImage
        
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage{
            imageForCrop = image
        }
        self.dismiss(animated: false) { [unowned self] in
                                  self.openEditor(nil, pickingViewTag: picker.view.tag)
                              }
        //imgProfile.image = imageForCrop
         //self.dismiss(animated:true, completion: nil)
    }
    func openEditor(_ sender: UIBarButtonItem?, pickingViewTag: Int) {
        guard let image = self.imageForCrop else {
            return
        }
        
        let cropViewController = CropViewController(image: image)
        cropViewController.setAspectRatioPreset(.presetSquare, animated: true)
         cropViewController.delegate = self
        self.present(cropViewController, animated: true, completion: nil)
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true)
    }
    @available(iOS 13.0, *)
    func pushToRootHomeViewController(){
        
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let VC  = storyboard.instantiateViewController(withIdentifier: "ContainerControllerVC") as! ContainerControllerVC
            let navigationController = UINavigationController(rootViewController:VC)
            let sceneDelegate = UIApplication.shared.connectedScenes
               .first!.delegate as! SceneDelegate
            sceneDelegate.window!.rootViewController = navigationController
    }
}


