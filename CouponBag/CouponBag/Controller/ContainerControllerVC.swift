//
//  ContainerControllerVC.swift
//  Coupon Bag
//
//  Created by MAC OS 17 on 25/02/22.
//

import UIKit
import SidebarOverlay

class ContainerControllerVC: SOContainerViewController {
    override var isSideViewControllerPresented: Bool {
        didSet {
            let action = isSideViewControllerPresented ? "opened" : "closed"
            let side = self.menuSide == .left ? "left" : "right"
            NSLog("You've \(action) the \(side) view controller.")
        }
    }
    override func viewDidLoad() {
            super.viewDidLoad()
        
        navigationController?.isNavigationBarHidden = true
        self.menuSide = .left
        self.topViewController = self.storyboard?.instantiateViewController(withIdentifier: "topScreen")
        self.sideViewController = self.storyboard?.instantiateViewController(withIdentifier: "SideNavigationVC")
    }
    }
    

