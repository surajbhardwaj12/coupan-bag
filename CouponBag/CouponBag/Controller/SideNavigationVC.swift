//
//  SideNavigationVC.swift
//  Coupon Bag
//
//  Created by MAC OS 17 on 25/02/22.
//

import UIKit

class SideNavigationVC: UIViewController {
    
    //MARK: - Outlet

    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var imgSideMenuprofile: UIImageView!
    @IBOutlet weak var btnCloseSideMenu: UIButton!
    @IBOutlet weak var sideMenuTableView: UITableView!
    
    //MARK: - Variable
    var sideMenuName: [String] = ["Home","Favourite Coupon","Collect Coupon","Update Password","Secure Pin","About Us","Invite User","Rate Us","Contact Us","Logout"]
    var sideMenuIcon: [UIImage] = [#imageLiteral(resourceName: "Home"),#imageLiteral(resourceName: "Favourite"),#imageLiteral(resourceName: "collectCoupon"),#imageLiteral(resourceName: "UpdatePassword"),#imageLiteral(resourceName: "SecurePin"),#imageLiteral(resourceName: "AboutUs"),#imageLiteral(resourceName: "InviteUser"),#imageLiteral(resourceName: "RateUS"),#imageLiteral(resourceName: "ContactUs"),#imageLiteral(resourceName: "Logout")]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.registerTableViewCell()
        sideMenuTableView.delegate = self
        sideMenuTableView.dataSource = self
        imgSideMenuprofile.setRoundImage()
        configureSidebar()
        
        
    }
    //MARK: - Custom Method
    func pushToRootHomeViewController(){
        
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let VC  = storyboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
            let navigationController = UINavigationController(rootViewController:VC)
            let sceneDelegate = UIApplication.shared.connectedScenes
               .first!.delegate as! SceneDelegate
            sceneDelegate.window!.rootViewController = navigationController
    }
    
    func configureSidebar() {
        
        if let userdetail = UserDetail.getUserFromUserDefault(){
            DispatchQueue.main.async { [self] in
                self.lblUserName.text = userdetail.name
                if let profileUrl = URL.init(string: userdetail.image){
                    self.imgSideMenuprofile.load(url: profileUrl)
                }
            }
        }
    }
    private func registerTableViewCell() {
        let labelCell = UINib(nibName: "SideMenuTableViewCell", bundle: nil)
        self.sideMenuTableView.register(labelCell, forCellReuseIdentifier: "SideMenuTableViewCell")
    }

    @IBAction func btnCloseSideMenu(_ sender: Any) {
        btnCloseSideMenu.isSelected.toggle()
        if let container = self.so_containerViewController {
               container.isSideViewControllerPresented = false
        }
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
// MARK:- Extension
extension SideNavigationVC : UITableViewDelegate,UITableViewDataSource,UICollectionViewDelegateFlowLayout{
    
   
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sideMenuName.count
      
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "SideMenuTableViewCell") as? SideMenuTableViewCell{
            
          cell.SideMenuNameList.text = sideMenuName[indexPath.row]
          cell.sideMenuBarIcon.image = sideMenuIcon[indexPath.row]
           
           
            
            return cell
            
        }
        return UITableViewCell()
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let tag = self.sideMenuName[indexPath.row]
        if tag == "Logout" {
            UIAlertController.showActionsheetForLogOut(self) { (btn,message) in
                switch btn{
                case 0:
                    UserDetail.removeUserFromUserDefault()
                    self.pushToRootHomeViewController()
                default:
                    print("cancel")
                }

            }
        }
}
}
extension UIImageView {
    func load(url: URL) {
        DispatchQueue.global().async { [weak self] in
            if let data = try? Data(contentsOf: url) {
                if let image = UIImage(data: data) {
                    DispatchQueue.main.async {
                        self?.image = image
                    }
                }
            }
        }
    }
}

