//
//  StoreViewController.swift
//  Coupon Bag
//
//  Created by MAC OS 17 on 15/02/22.
//

import UIKit
//import BadgeHub
class StoreViewController: UIViewController {
    
    //MARK: - Variable
    var storeImages: [UIImage] = [ #imageLiteral(resourceName: "KFC"),#imageLiteral(resourceName: "Burgerking"),#imageLiteral(resourceName: "chipotle"),#imageLiteral(resourceName: "Dominos"),#imageLiteral(resourceName: "dwolla"),#imageLiteral(resourceName: "mcdonalds"),#imageLiteral(resourceName: "pizzaHut"),#imageLiteral(resourceName: "starbucks") ]
//    var filterCount : BadgeHub?
    var storeList : [StoreListItem] = []
    var filterStorelist : [StoreListItem] = []
    var filterCategory = [Category]()
    var filterOn : Bool = false
   


   
    
    //MARK: - Outlet
    @IBOutlet weak var storeConstraints: NSLayoutConstraint!
    @IBOutlet weak var SearchBar: UISearchBar!
    @IBOutlet weak var EmptyUIView: UIView!
    @IBOutlet weak var SerchBar: UISearchBar!
    @IBOutlet weak var storeCollectionView: UICollectionView!

   
    var isShow:Bool = false
    override func viewDidLoad() {
        super.viewDidLoad()
        SerchBar.isHidden = true
        EmptyUIView.isHidden = true
        SearchBar.delegate =  self
        title = " Store List"
        setMyNavigation(titleColor: .white, color: UIColor(named: "BtnColor")!, titleSize: 25)
        let filterbutton = UIBarButtonItem(image: UIImage(named: "Filter"), style: .plain,target: self, action: nil)
        let searchButton = UIBarButtonItem(barButtonSystemItem: .search, target: self, action: #selector(showSearchBar))
        navigationItem.rightBarButtonItems = [filterbutton,searchButton]
        filterbutton.tintColor = UIColor.white
        searchButton.tintColor = UIColor.white
        self.storeCollectionView.delegate = self
        self.storeCollectionView.dataSource = self
        callStoreListApi(dict: nil, Count: nil)
        
        
    }

    //MARK: - Action Method
    @IBAction func btnAddStore(_ sender: Any) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "AddStoreViewController") as! AddStoreViewController
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    //MARK: - Custom Method
    @objc func showSearchBar() {
        isShow.toggle()
        if isShow {
            self.SearchBar.isHidden = false
        }else{
            self.SearchBar.isHidden = true
        }
        
    }
    
    //MARK: - Store List API
    func callStoreListApi(dict :[String:AnyObject]?,Count : Int?){
        
        APIRequestClient.shared.sendAPIRequest(requestType: .POST, queryString: dict != nil ? URL_FilteredStore : URL_StoreDetail , parameter: dict , isHudeShow: true, success: { (successResponse) in
            //print("response \(responseSuccess)")
            
            if let success = successResponse as? [String:Any] {
                if let storeInfo = success["data"] as? [AnyObject]{
                    
//                    print("store info \(storeInfo)")
//                    self.storeList.removeAll()
//                    self.filterStorelist.removeAll()
                    for jsonlist in storeInfo {
                        let list = StoreListItem(StoreDetail: jsonlist as! [String : AnyObject])

                        print(list.id)
                        print(list.stname)
                        print(list.stcontact)
                        self.storeList.append(list)
                        
                    }

                    DispatchQueue.main.async {
                        self.storeCollectionView.reloadData()
                    }
                    self.filterStorelist = self.storeList
                    DispatchQueue.main.async {
                        if Count != nil{
//                            self.filterCount?.increment(by: Count!)
//                            self.filterCount?.pop()
                        }
                    }
                  
                    
                    if dict != nil{
//                        self.filterOn = true
                        DispatchQueue.main.async {
//                            self.filterOption.setImage(UIImage(named: "filter-cancel"), for: .normal)
                        }
                       
                    }

                    if self.storeList.count <= 0{
                        DispatchQueue.main.async {
//                            self.emptyView.isHidden = false
                        }
                    }else{
                        DispatchQueue.main.async {
//                            self.searchViewBar.isHidden = false
//                            self.emptyView.isHidden = true
                        }
                    }
                    DispatchQueue.main.async {
                        self.storeCollectionView.reloadData()
                    }
                }else{
                    DispatchQueue.main.async {
//                        self.emptyView.isHidden = false
                    }
                    print("You Don't have any store")
                }
            }else{
                DispatchQueue.main.async {
//                    SAAlertBar.show(.error, message:"\(kCommonError)".localizedLowercase)
                }
            }
            
            DispatchQueue.main.async {
                self.storeCollectionView.reloadData()
            }

        }, fail: { (failResponse) in
            print(failResponse)
            if let Response = failResponse  as? [String:Any],let errorMessage = Response["message"] as? String{
                
                DispatchQueue.main.async {
                    
                    if errorMessage.count > 0{
//                        SAAlertBar.show(.error, message:"\(errorMessage)")

                    }
                }
            }else{
                DispatchQueue.main.async {
//                    SAAlertBar.show(.error, message:"\(kCommonError)".localizedLowercase)
                }
            }

        })
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
// MARK: - Extension
extension StoreViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return storeList.count
//        print(storeList.count)
//        return storeImages.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! StoreCollectionViewCell
        cell.lblStoreName.text = storeList[indexPath.row].stname
        if let profileUrl = URL.init(string: storeList[indexPath.row].store_img){
                    cell.imgStore.load(url: profileUrl)
        }
        return cell
    }
    
//    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
//        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! StoreCollectionViewCell
////        cell.imgStore.image = storeImages[indexPath.row]
//        cell.storeDetail = storeList[indexPath.row]
//        cell.indexID = indexPath.row
//
//        cell.lblStoreName.text = storeList[indexPath.row].stname.capitalized
//        if let profileUrl = URL.init(string: storeList[indexPath.row].store_img){
//            cell.imgStore.load(url: profileUrl)
//        }
//
////        if storeList[indexPath.row].is_favorite == "true"{
////          //  cell.favoriteImage.setBackgroundImage(UIImage(named: "favorite"), for: .normal)
////        }else
////        {
////           // cell.favoriteImage.setBackgroundImage(UIImage(named: "favorite-dark"), for: .normal)
////        }
////        //cell.delegate = self
////        //cell.layer.cornerRadius = 15.0
//
//        return cell
//    }

    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.frame.width - 30
        return CGSize(width: width/2, height: width/2)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 10, left: 0, bottom: 10, right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }

}
extension StoreViewController : UISearchBarDelegate{
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        print(searchText.count)
        var list = [StoreListItem]()
        if searchText.count > 0{
            list =  filterStorelist.filter { (store) -> Bool in
                if store.stname.lowercased().contains(searchText.lowercased()){
                    print(store.stname.lowercased())
                    print(searchText.lowercased())
                    return true
                }else{
                    return false
                }
            }
            storeList.removeAll()
            storeList = list
            DispatchQueue.main.async {
                self.storeCollectionView.reloadData()
            }
        }else{
            storeList = filterStorelist
            DispatchQueue.main.async {
                self.storeCollectionView.reloadData()
            }
        }
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        storeList = filterStorelist
        DispatchQueue.main.async {
            self.storeCollectionView.reloadData()
        }
    }
    
    
}
