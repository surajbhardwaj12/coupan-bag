//
//  Extension.swift
//  Coupon Bag
//
//  Created by MAC OS 17 on 01/03/22.
//

import Foundation
import UIKit
extension UIImageView {
    func setRoundImage() {
        layer.cornerRadius = layer.frame.height / 2
    }
}
extension UIButton {
    func setRoundButton() {
        layer.cornerRadius = layer.frame.height / 2
    }
}
