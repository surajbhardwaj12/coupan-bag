////
////  UIView+Extension.swift
////  1QuestionChild
////
////  Created by IPS on 25/06/21.
////
//
//import UIKit
//
////@IBDesignable
//class ShadowBackgroundView:UIView{
//    private var theShadowLayer: CAShapeLayer!
//
//    @IBInspectable open var rounding: Double = 15.0 {
//       didSet {
//           if rounding != oldValue {
//                self.layoutSubviews()
//           }
//       }
//   }
//
//
//    override func layoutSubviews() {
//        super.layoutSubviews()
//
//            let rounding = CGFloat.init(self.rounding)
//            var shadowLayer = CAShapeLayer.init()
//            shadowLayer.name = "ShadowLayer1"
//            shadowLayer.path = UIBezierPath.init(roundedRect: bounds, cornerRadius: rounding).cgPath
//            shadowLayer.fillColor = UIColor.white.cgColor
//            shadowLayer.shadowPath = shadowLayer.path
//        shadowLayer.shadowColor = UIColor.init(red: 60.0/255.0, green: 64.0/255.0, blue: 67.0/255.0, alpha:0.3).cgColor
//            shadowLayer.shadowRadius = CGFloat.init(2.0)
//            shadowLayer.shadowOpacity = Float.init(0.5)
//            shadowLayer.shadowOffset = CGSize.init(width: 0.0, height: 1.0)
//            if  let arraySublayer1:[CALayer] = self.layer.sublayers?.filter({$0.name == "ShadowLayer1"}),let sublayer1 =  arraySublayer1.first{
//                    sublayer1.removeFromSuperlayer()
//            }
//            self.layer.insertSublayer(shadowLayer, below: nil)
//            shadowLayer = CAShapeLayer.init()
//            shadowLayer.name = "ShadowLayer2"
//            shadowLayer.path = UIBezierPath.init(roundedRect: bounds, cornerRadius: rounding).cgPath
//            shadowLayer.fillColor = UIColor.white.cgColor
//            shadowLayer.shadowPath = shadowLayer.path
//        shadowLayer.shadowColor = UIColor.init(red: 60.0/255.0, green: 64.0/255.0, blue: 67.0/255.0, alpha:0.15).cgColor
//            shadowLayer.shadowRadius = CGFloat.init(6.0)
//            shadowLayer.shadowOpacity = Float.init(0.5)
//            shadowLayer.shadowOffset = CGSize.init(width: 0.0, height: 2.0)
//            if  let arraySublayer2:[CALayer] = self.layer.sublayers?.filter({$0.name == "ShadowLayer2"}),let sublayer2 =  arraySublayer2.first{
//                sublayer2.removeFromSuperlayer()
//            }
//            self.layer.insertSublayer(shadowLayer, below: nil)
//
//    }
//}
//
//// MARK: - Extension ( Style - Corner, Border-Color, Border-Width )
//extension UIView{
//
//    //Mark : Action Method Function
//        typealias ReturnGestureAction = (() -> Void)?
//        fileprivate struct AssociatedObjectKeys {
//            static var tapGestureRecognizer = "MediaViewerAssociatedObjectKey_mediaViewer1"
//        }
//
//
//        var tapGestureRecognizerAction: ReturnGestureAction? {
//            set {
//                if let newValue = newValue {
//                    // Computed properties get stored as associated objects
//                    objc_setAssociatedObject(self, &AssociatedObjectKeys.tapGestureRecognizer, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
//                }
//            }
//            get {
//                let tapGestureRecognizerActionInstance = objc_getAssociatedObject(self, &AssociatedObjectKeys.tapGestureRecognizer) as? ReturnGestureAction
//                return tapGestureRecognizerActionInstance
//            }
//        }
//
//        func handleControlTabAction(action: (() -> Void)?) {
//            let gesture = UITapGestureRecognizer(target: self, action: #selector(self.tapGestureHanldeAction))
//            self.tapGestureRecognizerAction = action
//            self.isUserInteractionEnabled = true
//            self.addGestureRecognizer(gesture)
//        }
//    @objc func tapGestureHanldeAction() {
//           if let action = self.tapGestureRecognizerAction {
//               action?()
//           } else {
//               print("no action")
//           }
//       }
//    @IBInspectable var cornerRadius: CGFloat{
//        get{ return self.cornerRadius}
//        set{
//            self.layer.cornerRadius = newValue
//        }
//    }
//    @IBInspectable var borderColor: UIColor? {
//        get {
//            guard let cgColor = layer.borderColor else {
//                return nil
//            }
//            return UIColor(cgColor: cgColor)
//        }
//        set { layer.borderColor = newValue?.cgColor }
//    }
//    @IBInspectable var borderWidth: CGFloat {
//        get {
//            return layer.borderWidth
//        }
//        set {
//            layer.borderWidth = newValue
//        }
//    }
//    func dropShadow(scale: Bool = true) {
//        layer.masksToBounds = false
//        layer.shadowColor = UIColor.darkGray.cgColor
//        layer.shadowOpacity = 0.7
//        layer.shadowOffset = CGSize(width: 1, height: -1)
//        layer.shadowRadius = 10
//
//        //layer.shadowPath = UIBezierPath(rect: bounds).cgPath
//        layer.shouldRasterize = true
//        layer.rasterizationScale = scale ? UIScreen.main.scale : 1
//      }
//}
//
