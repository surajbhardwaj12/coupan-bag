//
//  NavigationControllerExtension.swift
//  NavigationControl
//
//  Created by MAC OS 17 on 21/02/22.
//

import Foundation
import UIKit

//enum FontsStyle: String {
//    case Normal = ""
//    case Regular = "Regular"
//    case Bold = "Bold"
//    case BoldItalic = "BoldItalic"
//}
enum Fonts: String {
    case Black                 = "Roboto-Black"
    case Black_Oblique         = "AvenirBlack_Oblique"
    case Bold                  = "Roboto-Bold"
    case Heavy                 = "Avenir-Heavy"
    case light                 = "Roboto-Light"
    case Medium                = "Avenir-Medium"
    case Medium_Oblique        = "Avenir-Medium_Oblique"
}


//enum Fonts {
//
//
//    case HelveticaNeue(FontsStyle)
//
//
//}

extension UIViewController {
    func setMyNavigation(titleColor: UIColor, color : UIColor, titleSize: Int) {
  
        
        let appearance = UINavigationBarAppearance()
        appearance.configureWithOpaqueBackground()
        appearance.backgroundColor = color
        appearance.titleTextAttributes = [.foregroundColor: titleColor]
        navigationItem.standardAppearance = appearance
        navigationItem.scrollEdgeAppearance = appearance
        navigationItem.compactAppearance = appearance
        let leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "menu"), style: .plain,target: self, action: #selector(sidemenu))
        //let lefttButton2 = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(clickedAdd))
        navigationItem.leftBarButtonItem = leftBarButtonItem
        leftBarButtonItem.tintColor = UIColor.white
//        rightButton2.tintColor = UIColor.white
//        let button = UIButton(type: UIButton.ButtonType.custom)
//        button.setImage(UIImage(named: "menu"), for: .normal)
//        button.addTarget(self, action:#selector(sidemenu), for: .touchDragInside)
//        button.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
//        let barButton = UIBarButtonItem(customView: button)
//        barButton.tintColor = UIColor.white
//        navigationItem.leftBarButtonItems = [barButton]
        
    }
    @objc func sidemenu(){
        if let container = self.so_containerViewController {
           container.isSideViewControllerPresented = true
        }
               
    }
    

}

