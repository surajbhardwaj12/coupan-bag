//
//  CardListItem.swift
//  Stocard
//
//  Created by IPS on 26/05/21.
//

import Foundation
class CardListItem: NSObject {
    var id:Int = 0
    var cardname:String = ""
    var rewardpercen:String = ""
    var expdate:String = ""
    var cardDescription:String = ""
    var card_img:String = ""
    var status:String = ""
    var isActive:String = ""
    var cardno:String = ""
    var isUsed:String = ""
    
    
    init(CardDetail : [String:Any]) {
            super.init()
            if let id = CardDetail["id"]{
                self.id = Int(id as! Int64)
            }
    
            if let cardname = CardDetail["cardname"]{
                self.cardname = "\(cardname)"
            }
            if let expdate = CardDetail["expdate"]{
                self.expdate = "\(expdate)"
            }
            if let description = CardDetail["carddetail"]{
                self.cardDescription = "\(description)"
            }
            if let rewardpercen = CardDetail["rewardpercen"]{
                self.rewardpercen = "\(rewardpercen)"
            }
            if let carddetail = CardDetail["carddetail"]{
                self.cardDescription = "\(carddetail)"
            }
            if let card_img = CardDetail["card_img"]{
                self.card_img = "\(card_img)"
            }
            if let status = CardDetail["status"]{
                self.status = "\(status)"
            }
            if let isActive = CardDetail["isActive"]{
                self.isActive = "\(isActive)"
            }
            if let cardno = CardDetail["cardno"]{
                self.cardno = "\(cardno)"
            }
            if let isUsed = CardDetail["is_Used"]{
                self.isUsed = "\(isUsed)"
            }
        }
}
