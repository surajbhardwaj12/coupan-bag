import Foundation
class StoreListItem : NSObject{
    
    var id : Int64 = 0
    var stname :String = ""
    var stlocation : String = ""
    var stcontact : String = ""
    var store_img : String = ""
    var is_favorite : String = ""
    
    init(StoreDetail : [String:Any]) {
            super.init()
            if let id = StoreDetail["id"]{
                self.id = id as! Int64
            }
    
            if let stname = StoreDetail["stname"]{
                self.stname = "\(stname)"
            }
            if let stlocation = StoreDetail["stlocation"]{
                self.stlocation = "\(stlocation)"
            }
            if let stcontact = StoreDetail["stcontact"]{
                self.stcontact = "\(stcontact)"
            }
            if let image = StoreDetail["store_img"]{
                self.store_img = "\(image)"
            }
            if let isfavorite = StoreDetail["is_favorite"]{
                self.is_favorite = "\(isfavorite)"
            }
        }
}
   

